//
//  TableTopic+CoreDataProperties.swift
//  Topic Master
//
//  Created by William Kluss on 5/8/20.
//
//

import Foundation
import CoreData


extension TableTopic {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TableTopic> {
        return NSFetchRequest<TableTopic>(entityName: "TableTopic")
    }

    @NSManaged public var topicCategory: String?
    @NSManaged public var topicDescription: String?
    @NSManaged public var category: Category?

}
