//
//  UIColor+TableTopics.swift
//  TableTopics
//
//  Created by William Kluss on 7/16/16.
//
//

import UIKit

extension UIColor {
    static func tableTopicGreen() -> UIColor {
        return UIColor(red: (34 / 255.0), green: (139 / 255.0), blue: (34 / 255.0), alpha: 1.0)
    }
}