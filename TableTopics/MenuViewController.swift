//
//  MenuViewController.swift
//  TableTopics
//
//  Created by William Kluss on 5/23/16.
//
//

import UIKit
import MessageUI
import CloudKit

class MenuViewController: UIViewController {
    private let settingManger = SettingsManager()
    private let notificationCenter = NotificationCenter.default
    private let analyticManager = AnalyticManager.sharedInstance
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func doneButtonPressed(_ sender: AnyObject) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func contactButtonPressed(_ sender: UIButton) {
        analyticManager.trackEvent(AnalyticKeys.EventAction.contactTapped.rawValue, parameters: nil)
        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setSubject("Topic Master Feedback")
        mailVC.setToRecipients(["klussdevelopment@gmail.com"])
        
        self.present(mailVC, animated: true, completion: nil)
    }
    
    @IBAction func topicButtonPressed(_ sender: Any) {
        analyticManager.trackEvent(AnalyticKeys.EventAction.topicsTapped.rawValue, parameters: nil)
    }
    
    @IBAction func timerSettingsTapped(_ sender: UIButton) {
        analyticManager.trackEvent(AnalyticKeys.EventAction.settingsTapped.rawValue, parameters: nil)

        let timerAlertController = UIAlertController(title: "Timer Options", message: nil, preferredStyle: .actionSheet)
        
        let cancelAction = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
        }
        let thirtyAction = UIAlertAction.init(title: "Thirty Seconds", style: .default) { (action) in
            
            self.analyticManager.trackEvent(AnalyticKeys.EventAction.settingsTapped.rawValue, parameters: [AnalyticKeys.Category.timeOption.rawValue: AnalyticKeys.EventAction.thirtySecondsTapped.rawValue])
            self.settingManger.saveTimerSettings(.thirty)
            self.notificationCenter.post(name: Notification.Name.timerSettingUpdate, object: nil)
        }
        let sixtyAction = UIAlertAction.init(title: "Sixty Seconds", style: .default) { (action) in
            self.analyticManager.trackEvent(AnalyticKeys.EventAction.settingsTapped.rawValue, parameters: [AnalyticKeys.Category.timeOption.rawValue: AnalyticKeys.EventAction.sixtySecondsTapped.rawValue])
            self.settingManger.saveTimerSettings(.sixty)
            self.notificationCenter.post(name: Notification.Name.timerSettingUpdate, object: nil)

        }
        let ninetyAction = UIAlertAction.init(title: "Ninety Seconds", style: .default) { (action) in
            self.analyticManager.trackEvent(AnalyticKeys.EventAction.settingsTapped.rawValue, parameters: [AnalyticKeys.Category.timeOption.rawValue: AnalyticKeys.EventAction.ninetySecondsTapped.rawValue])
            self.settingManger.saveTimerSettings(.ninety)
            self.notificationCenter.post(name: Notification.Name.timerSettingUpdate, object: nil)
        }
        
        timerAlertController.addAction(cancelAction)
        timerAlertController.addAction(thirtyAction)
        timerAlertController.addAction(sixtyAction)
        timerAlertController.addAction(ninetyAction)
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            timerAlertController.modalPresentationStyle = .pageSheet
            timerAlertController.popoverPresentationController?.sourceRect = sender.bounds
            timerAlertController.popoverPresentationController?.sourceView = sender
        }
        
        navigationController?.present(timerAlertController, animated: true, completion: nil)
        
    }
    @IBAction func buyTopicPacksPressed(_ sender: Any) {
        let viewController = TopicPackViewController()
        navigationController?.pushViewController(viewController, animated: true)
        
    }
}

extension MenuViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension Notification.Name {
    public static let timerSettingUpdate = Notification.Name("timerSettingUpdate")
    public static let topicStatusUpdated = Notification.Name("topicUpdated")
}
