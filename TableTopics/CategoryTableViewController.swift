//
//  CategoryTableViewController.swift
//  TableTopics
//
//  Created by William Kluss on 6/2/16.
//
//

import UIKit

class CategoryTableViewController: UITableViewController, UITextFieldDelegate {

    fileprivate var categories: [Category] = []
    fileprivate let tableTopicService = TableTopicService()
    private let notificationCenter = NotificationCenter.default
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadTableData()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

    @IBAction func addCategoryButtonPressed(_ sender: UIBarButtonItem) {
        let categoryAlertBox = UIAlertController(title: "Enter Category Name", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        categoryAlertBox.addTextField { (textField: UITextField) in
            textField.delegate = self
            textField.autocapitalizationType = .sentences
            textField.addTarget(self, action: #selector(CategoryTableViewController.categoryNameTextFieldDidChange), for: UIControl.Event.editingChanged)
        }
        
        let addCategoryAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default) { (action: UIAlertAction) in
            if let textField = categoryAlertBox.textFields?.first {
                if let title = textField.text {
                    do {
                        AnalyticManager.sharedInstance.trackEvent(AnalyticKeys.EventAction.saveCategory.rawValue, parameters: [AnalyticKeys.EventAction.addCategory.rawValue : title])
                        try self.tableTopicService.createCategory(title)
                        self.reloadTableData()
                    } catch {
                        NSLog("Unable to create Category")
                    }
                }
            }
        }
        
        addCategoryAction.isEnabled = false
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        categoryAlertBox.addAction(addCategoryAction)
        categoryAlertBox.addAction(cancelAction)
        
        present(categoryAlertBox, animated: true, completion: nil)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath) as UITableViewCell
        let category = categories[indexPath.row]

        // Configure the cell...
        cell.textLabel?.text = category.title
        
        if let numberOfTopics = category.tabletopics?.count {
            cell.detailTextLabel?.text = "Number of Topics: \(numberOfTopics)"
        }

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let categoryToDelete = categories[indexPath.row]
            let deleteAlert = UIAlertController(title: "Delete Category", message: "Deleting a category will delete all the topics associated to it.", preferredStyle: UIAlertController.Style.alert)
            
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
            let deleteAction = UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive, handler: { (action: UIAlertAction) in
                do {
                    try self.tableTopicService.deleteCategory(categoryToDelete)
                    self.reloadTableData()
                    self.notificationCenter.post(name: Notification.Name.topicStatusUpdated, object: nil)
                } catch {
                    NSLog("Unable to delete")
                }
            })
    
            deleteAlert.addAction(deleteAction)
            deleteAlert.addAction(cancelAction)
            
            present(deleteAlert, animated: true, completion: nil)
        }
    }
    
    @objc fileprivate func categoryNameTextFieldDidChange() -> Void {
        if let categoryAlertBox = presentedViewController as? UIAlertController {
            if let textField = categoryAlertBox.textFields?.first {
                if let action = categoryAlertBox.actions.first {
                    if textField.text?.isEmpty == false {
                        action.isEnabled = true
                    } else {
                        action.isEnabled = false
                    }
                }
            }
        }
    }
    
   fileprivate func reloadTableData() -> Void {
        do {
            categories = try tableTopicService.retrieveAllCategories()
            tableView.reloadData()
        } catch {
            NSLog("Unable to retrieve Categories")
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "tableTopicSegue" {
            let topiceCollectionVC = segue.destination as! TableTopicCollectionViewController
            //get selected cell index
            if let indextPath = tableView.indexPathForSelectedRow {
                let category = categories[indextPath.row]
                topiceCollectionVC.category = category
            }
        }
    }
}
