//
//  MembersTransformationPolicy.swift
//  TableTopics
//
//  Created by William Kluss on 5/23/16.
//
//

import UIKit
import Foundation
import CoreData

class MembersTransformationPolicy: NSEntityMigrationPolicy {
    
    func memberNameFromEntity(_ member: Member) -> String {
        guard let firstName = member.firstName, let lastName = member.lastName else {
            return ""
        }
        
        return firstName + " " + lastName
    }
}
