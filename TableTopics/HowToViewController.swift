//
//  HowToViewController.swift
//  TableTopics
//
//  Created by William Kluss on 5/23/16.
//
//

import UIKit

class HowToViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    @IBAction func doneButtonPressed(_ sender: UIBarButtonItem){
        navigationController?.dismiss(animated: true, completion: nil)
    }
}
