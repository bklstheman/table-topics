//
//  AddTableTopicViewController.swift
//  TableTopics
//
//  Created by William Kluss on 6/4/16.
//
//

import UIKit

protocol AddTableTopicViewControllerDelegate: class {
    func tableTopicWasSaved(_ category: Category)
}

//TODO: DELETE FILE
class AddTableTopicViewController: UIViewController, UITextViewDelegate {

    @IBOutlet fileprivate weak var topicTextView: UITextView!
    @IBOutlet fileprivate weak var saveButton: UIBarButtonItem!
    
    fileprivate let tableTopicService = TableTopicService()
    weak var delegate: AddTableTopicViewControllerDelegate?
    var category: Category!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topicTextView.delegate = self
        topicTextView.becomeFirstResponder()
        saveButton.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @IBAction func saveButtonPressed(_ sender: UIBarButtonItem) {
        do {
            try tableTopicService.createTableTopic(topicTextView.text, category: category)
            delegate?.tableTopicWasSaved(category)
            navigationController?.popViewController(animated: true)
        } catch {
            NSLog("Unable to save" )
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.count > 0 {
            saveButton.isEnabled = true
        } else {
            saveButton.isEnabled = false
        }
    }
}
