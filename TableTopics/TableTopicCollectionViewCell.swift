//
//  TableTopicCollectionViewCell.swift
//  TableTopics
//
//  Created by William Kluss on 6/4/16.
//
//

import UIKit

protocol TableTopicCollectionViewCellDelegate: class {
    func deleteTableTopicWasPressed(_ tableTopicCell: TableTopicCollectionViewCell) -> Void
}

class TableTopicCollectionViewCell: UICollectionViewCell {

    @IBInspectable var cornerRadius: CGFloat = 5
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    @IBOutlet weak var topicDescription: UILabel!
    weak var delegate: TableTopicCollectionViewCellDelegate?
    
    @IBAction func deleteTopicButtonPressed(_ sender: UIButton) {
        delegate?.deleteTableTopicWasPressed(self)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)

        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
}
