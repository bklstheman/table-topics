//
//  TopicPackViewController.swift
//  Topic Master
//
//  Created by William Kluss on 5/6/20.
//

import UIKit
import SwiftEntryKit
import StoreKit

class TopicPackViewController: UIViewController {
    
    struct ViewModel {
        private enum Row: Int, CaseIterable {
            case buyAll
            case iceBreakerPacks
            case allIceBreakerPacks
            case personalPacks
            case allPersonalPack
            case deepQuestionPacks
            case allDeepQuestionPacks
            case healthAndEducationPacks
            case controversial
        }
        
        let rowCount = ViewModel.Row.allCases.count
        
        func topicPackIds(for rowIndex: Int) -> [TopicPackProductId] {
            let row = ViewModel.Row(rawValue: rowIndex)!
            switch row {
            case .buyAll:
                return  [.allProducts]
            case .personalPacks:
                return [.personal1, .personal2]
            case .allPersonalPack:
                return [.personalAll]
            case .healthAndEducationPacks:
                return [.health, .education]
            case .deepQuestionPacks:
                return [.deepQuestion1, .deepQuestion2]
            case .allDeepQuestionPacks:
                return [.deepQuestionAll]
            case .iceBreakerPacks:
                return [.iceBreaker1, .iceBreaker2]
            case .allIceBreakerPacks:
                return [.iceBreakerAll]
            case .controversial:
                return [.controversial]
            }
        }
        
        func title(for product: TopicPackProductId) -> String {
            switch product {
            case .health:
                return "Health Pack"
            case .personal1:
                return "Personal Pack 1"
            case .personal2:
                return "Personal Pack 2"
            case .deepQuestion1:
                return "Deep Question Pack 1"
            case .deepQuestion2:
                return "Deep Question Pack 2"
            case .education:
                return "Education Pack"
            case .controversial:
                return "Controversial Pack"
            case .personalAll:
                return "Personal Pack 1 & 2"
            case .deepQuestionAll:
                return "Deep Question Pack 1 & 2"
            case .allProducts:
                return "All Products"
            case .iceBreakerAll:
                return "Ice Breaker Pack 1 & 2"
            case .iceBreaker1:
                return "Ice Breaker Pack 1"
            case .iceBreaker2:
                return "Ice Breaker Pack 2"
            }
        }
    }
    
    private let importer = TableMasterImporter()
    private let tableView: UITableView = UITableView(frame: CGRect.zero, style:.plain)
    private let viewModel: TopicPackViewController.ViewModel
    private let iapHelper = IAPHelper()
    private let analyticManager = AnalyticManager.sharedInstance
    
    init(viewModel: TopicPackViewController.ViewModel = TopicPackViewController.ViewModel()) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
        self.iapHelper.delegate = self
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        setupTableView()
        setupRestoreButton()
        title = "Buy Topics Packs"
        tableView.backgroundColor = .systemGroupedBackground
    }
    
    private func setupRestoreButton() {
        let rightBarButton = UIBarButtonItem(title: "Restore", style: .done, target: self, action: #selector(restorePurchases))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    @objc private func restorePurchases() {
        iapHelper.restorePurchases()
    }
    
    private func setupTableView() {
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(tableView)
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.estimatedRowHeight = 55.0
        tableView.separatorStyle = .none
        
        tableView.tableFooterView = UIView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        
        tableView.register(TopicPackTableViewCell.self, forCellReuseIdentifier: TopicPackTableViewCell.cellIdentifier)
    }
}

extension TopicPackViewController: UITableViewDelegate {
    
}

extension TopicPackViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let productIds = viewModel.topicPackIds(for: indexPath.row)
        let cell = TopicPackTableViewCell(productIds: productIds, delegate: self)
        cell.backgroundColor = .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}

extension TopicPackViewController: TopicPackViewDelegate {
    func didPressTopic(_ value: TopicPackProductId) {
        analyticManager.trackEvent("Topic Pack Pressed", parameters: ["pack": value.title()])
        
        let viewModel = TopicBuyPackViewController.ViewModel(productId: value)
        let viewController = TopicBuyPackViewController(viewModel: viewModel)
        let attributes = setupEKAttributes()
        
        SwiftEntryKit.display(entry: viewController, using: attributes)
    }
    
    private func setupEKAttributes() -> EKAttributes {
        var attributes = EKAttributes()
        attributes.position = .center
        attributes.displayMode = .light
        attributes.displayDuration = .infinity
        attributes.roundCorners = .all(radius: 25)
        attributes.border = .value(color: .black, width: 1.0)
        attributes.screenBackground = .visualEffect(style: .dark)
        attributes.screenInteraction = .dismiss
        attributes.entryInteraction = .forward //This this will forward to the view controller
        
        
        //setup frame
        let widthRation: CGFloat = UIDevice.current.userInterfaceIdiom == .phone ? 0.9 : 0.5
        let heigthRation: CGFloat = UIDevice.current.userInterfaceIdiom == .phone ? 0.5 : 0.3
        let widthConstraint = EKAttributes.PositionConstraints.Edge.ratio(value: widthRation)
        let heigthConstraint = EKAttributes.PositionConstraints.Edge.ratio(value: heigthRation) //might need to tweak this
        attributes.positionConstraints.size = .init(width: widthConstraint, height: heigthConstraint)
        
        
        return attributes
    }
}

extension TopicPackViewController: PaymentDelegate {
    func paymentProcessFinished(transaction: SKPaymentTransaction) {
        //No op
    }
    
    func restoreProcessFinished(transactions: [SKPaymentTransaction]) {
        
        guard transactions.count > 0 else { return }
        var packsRestored: [String] = []
        for transaction in transactions {
            if let product = TopicPackProductId(rawValue: transaction.payment.productIdentifier) {
                do {
                    try importer.importTableMaster(types: product.tableMasterOption())
                    packsRestored.append(product.title())
                } catch {
                    //failed to restore, show alert
                    errorAlert()
                    analyticManager.trackEvent("Topic Pack Restored failed", parameters: nil)
                    break
                }
            }
        }
        analyticManager.trackEvent("Topic Pack Restored success", parameters: nil)
        successAlert(restoredPacks: packsRestored)
    }
    
    private func errorAlert() {
        let alert = UIAlertController(title: "Not able to restore", message: "Unable to restore your purchase. Please try again.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func successAlert(restoredPacks: [String]) {
        let alert = UIAlertController(title: "Restore Success", message: "These packs were restored: \(restoredPacks.joined(separator: ", ")).", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Great", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}
