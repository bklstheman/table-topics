//
//  TopicPackTableViewCell.swift
//  Topic Master
//
//  Created by William Kluss on 5/7/20.
//

import UIKit

class TopicPackTableViewCell: BasicTableViewCell {

    static let cellIdentifier: String = "TopicPackTableViewCell"
    private let productIds: [TopicPackProductId]

    private weak var delegate: TopicPackViewDelegate?
    
    init(productIds: [TopicPackProductId], delegate: TopicPackViewDelegate?) {
        self.productIds = productIds
        self.delegate = delegate
        super.init(style: .default, reuseIdentifier: TopicPackTableViewCell.cellIdentifier)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        let viewModel = TopicPackViewContainer.ViewModel(productIds: productIds)
        let containerView = TopicPackViewContainer(viewModel: viewModel, delegate: delegate)
        addView(containerView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
}
