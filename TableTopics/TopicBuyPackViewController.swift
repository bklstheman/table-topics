//
//  TopicBuyPackViewController.swift
//  Topic Master
//
//  Created by William Kluss on 5/7/20.
//

import UIKit
import StoreKit
import SwiftEntryKit

class TopicBuyPackViewController: UIViewController {
    
    class ViewModel {
        private let productId: TopicPackProductId
        private let iapHelper: IAPHelper = IAPHelper()
        private let packImporter = TableMasterImporter()
        private let notificationCenter = NotificationCenter.default
        
        var productPriceReturnedCallback: ((String) -> ())?
        var cannotMakePurchase: (() -> ())?
        var importComplete: (() -> ())?
        
        private lazy var priceFormatter: NumberFormatter = {
            let formatter = NumberFormatter()
            formatter.locale = Locale.current
            formatter.numberStyle = .currency
            
            return formatter
        }()
        
        init(productId: TopicPackProductId) {
            self.productId = productId
            iapHelper.delegate = self
        }
        lazy var title: String = {
            return productId.title()
        }()
        
        lazy var description: String = {
            switch productId {
            case .health:
                return "Contains over 20 different topics centered around health and fitness."
            case .personal1:
                return "Contains over 20 different topics that ask personal style questions."
            case .personal2:
                return "Contains over 20 different topics that ask personal style questions."
            case .deepQuestion1:
                return "Contains over 25 different topics that ask deep and introspective questions."
            case .deepQuestion2:
                return "Contains over 25 different topics that ask deep and introspective questions."
            case .education:
                return "Contains over 20 different topics centered around education."
            case .controversial:
                return "Contains over 20 different topics that can be contentious to ask."
            case .personalAll:
                return "Contains all of Personal 1 & 2 packs which contains over 40 topics."
            case .deepQuestionAll:
                return "Contains all of Deep Question 1 & 2 packs which contains over 50 topics."
            case .allProducts:
                return "Includes all the packs, containing over 190 topics"
            case .iceBreakerAll:
                return "Contains all of Ice Breaker 1 & 2 packs which contains over 40 topics."
            case .iceBreaker1:
                return "Contains over 20 different topics centered team building and warming up."
            case .iceBreaker2:
                return "Contains over 20 different topics centered team building and warming up."
            }
        }()
        
        func fetchPrice() {
            iapHelper.requestProduct(productId: productId.rawValue) { [weak self] (result) in
                guard let self = self else { return }
                
                switch result {
                case .success(let products):
                    
                    guard let product = products.first else { return }
                    var price = self.priceFormatter.string(from: product.price)!
                    
                    if  let discountPrice = product.discounts.first?.price {
                        price = self.priceFormatter.string(from: discountPrice)!
                    }
                    
                    let text = "Buy\n\(price)"
                    self.productPriceReturnedCallback?(text)
                case .failure(_):
                    break
                }
            }
        }
        
        func buy() {
            guard iapHelper.canMakePayments() == true else {
                cannotMakePurchase?()
                return
            }
            
            let payment = SKMutablePayment()
            payment.productIdentifier = productId.rawValue
            iapHelper.buy(payment)
            
            AnalyticManager.sharedInstance.trackEvent("Topic Pack Buying", parameters: ["pack": productId.title()])
        }
        
        func startImportProcess() {
            do {
                try packImporter.importTableMaster(types: productId.tableMasterOption())
                notificationCenter.post(name: Notification.Name.topicStatusUpdated, object: nil)
                importComplete?()
            } catch {
                cannotMakePurchase?()
            }
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var priceButton: CardButton!
    
    private var viewModel: TopicBuyPackViewController.ViewModel
    
    init(viewModel: TopicBuyPackViewController.ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: TopicBuyPackViewController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        viewModel.fetchPrice()
    }
    
    private func setup() {
        view.backgroundColor = .systemGroupedBackground
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.description
        priceButton.titleLabel?.numberOfLines = 0
        priceButton.titleLabel?.textAlignment = .center
        priceButton.setTitleColor(.white, for: .normal)
        priceButton.backgroundColor = UIColor.tableTopicGreen()
        priceButton.titleLabel?.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        
        setupViewModel()
    }
    
    private func setupViewModel() {
        viewModel.cannotMakePurchase = { [weak self] in
            self?.displayAlertMessage()
        }
        
        viewModel.productPriceReturnedCallback = { [weak self] (title) in
            guard let self = self else { return }
            DispatchQueue.main.async {
                self.priceButton.setTitle(title, for: .normal)
                self.priceButton.titleLabel?.sizeToFit()
            }
        }
        
        viewModel.importComplete = {
            SwiftEntryKit.dismiss()
        }
    }
    
    private func displayAlertMessage() {
        let alert = UIAlertController(title: "Not able to complete", message: "Unable to make purchases. Please check your settings.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
    @IBAction func closeButtonTapped(_ sender: Any) {
        SwiftEntryKit.dismiss()
    }
    
    @IBAction func priceButtonPressed(_ sender: Any) {
        viewModel.buy()
    }
}

extension TopicBuyPackViewController.ViewModel: PaymentDelegate {
    func restoreProcessFinished(transactions: [SKPaymentTransaction]) {
        //Ignore
    }
    
    func paymentProcessFinished(transaction: SKPaymentTransaction) {
        //If transaction goes through, kick off import for id.
        switch transaction.transactionState {
        case .purchasing:
            break
        case .purchased:
            startImportProcess()
        case .failed:
            cannotMakePurchase?()
        break //notify
        case .restored:
            startImportProcess()
        break //start the importer. Check to see what was purchased
        case .deferred:
            break
        @unknown default:
            break
        }
    }
    
    
}
