//
//  TableMasterImporter.swift
//  Topic Master
//
//  Created by William Kluss on 5/5/20.
//

import Foundation

enum ImporterError: Error {
    case unableToFindURL
    case unableToDecode
    case unableToComplete
}

struct TableMasterImport: Codable {
    let title: String
    let topics: [String]
}

class TableMasterImporter {
    private let tableTopicService = TableTopicService()

    func importTableMaster(types: [TableMasterOption]) throws {
        for pack in types {
            try importTableMaster(type: pack)
        }
    }
    func importTableMaster(type: TableMasterOption) throws {
        do {
            let importData = try fetchTableMasterValues(type: type)
            guard let category = try? tableTopicService.createCategory(importData.title) else { throw ImporterError.unableToComplete }
        
            for item in importData.topics.shuffled() {
                try tableTopicService.createTableTopic(item, category: category)
            }
        } catch {
            throw ImporterError.unableToComplete
        }
    }
    
    func fetchTableMasterValues(type: TableMasterOption) throws -> TableMasterImport {
        guard let url = type.fileURL(), let data = try? Data(contentsOf: url) else { throw ImporterError.unableToFindURL }
        guard let masterImport = try? JSONDecoder().decode(TableMasterImport.self, from: data) else { throw ImporterError.unableToDecode }
        
        return masterImport
    }
    
    func fetchAllTableMasterPacks() throws -> [TableMasterImport] {
        var packs: [TableMasterImport] = []
        for option in TableMasterOption.allCases {
            let pack = try fetchTableMasterValues(type: option)
            packs.append(pack)
        }
        return packs
    }
}

enum TableMasterOption: String, CaseIterable {
    case health = "HealthFinal"
    case personal1 = "Personal1Final"
    case personal2 = "Personal2Final"
    case deepQuestion1 = "DeepQuestions1Final"
    case deepQuestion2 = "DeepQuestions2Final"
    case education = "EducationFinal"
    case iceBreaker1 = "IceBreakers1Final"
    case iceBreaker2 = "IceBreakers2Final"
    case controversial = "ControversialFinal"
    
    func fileURL() -> URL? {
        guard let path = Bundle.main.path(forResource: self.rawValue, ofType: "json") else { return nil }
        let url = URL(fileURLWithPath: path)
        return url
    }
}
