//
//  TableTopicService.swift
//  TableTopics
//
//  Created by William Kluss on 6/2/16.
//
//

import Foundation
import CoreData

class TableTopicService {
    
    fileprivate let managedContext = CoreDataManager.sharedInstance.managedObjectContext
    fileprivate let categoryFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Category")
    fileprivate let tableTopicFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "TableTopic")
    fileprivate let categorySortDescriptor = NSSortDescriptor(key: "title", ascending: true)
    
    func retrieveAllCategories() throws -> [Category] {
        let fetchRequest = categoryFetch
        fetchRequest.sortDescriptors = [categorySortDescriptor]
        
        let results = try managedContext.fetch(fetchRequest)
        
        if let categories = results as? [Category] {
            return categories
        } else {
            NSLog("Retrieved wrong type of category")
            return []
        }
    }
    
    func retrieveCategoy(_ title: String) throws -> Category? {
    
        let fetchRequest = categoryFetch
        fetchRequest.predicate = NSPredicate(format: "title == %@", title)
        
        let results = try managedContext.fetch(fetchRequest)
        
        if let categories = results as? [Category], results.count > 0 {
            return categories.first! 
        } else {
            return nil
        }
    }
    
    @discardableResult func createCategory(_ title: String) throws -> Category?  {
        let newCategory = NSEntityDescription.insertNewObject(forEntityName: "Category", into: managedContext) as? Category
        
        newCategory?.title = title
        
        try managedContext.save()
        
        return newCategory
    }
    
    func deleteCategory(_ category: Category) throws {
        managedContext.delete(category)
        
        try managedContext.save()
    }
    
    func createTableTopic(_ tableTopic: String, category: Category) throws {
        let newTableTopic = NSEntityDescription.insertNewObject(forEntityName: "TableTopic", into: managedContext) as? TableTopic
        
        newTableTopic?.category = category
        newTableTopic?.topicDescription = tableTopic
        
        try managedContext.save()
    }
    
    func deleteTableTopic(_ tableTopic: TableTopic) throws {
        managedContext.delete(tableTopic)
        
        try managedContext.save()
    }
        
    func retrieveAllTopics() throws -> [TableTopic]{
        do {
            var allTableTopics: [TableTopic] = []
            let categories = try retrieveAllCategories()
            
            for category in categories {
                if let topics = category.tabletopics?.allObjects as? [TableTopic] {
                    allTableTopics.append(contentsOf: topics)
                }
            }
            return allTableTopics
        } catch {
            return []
        }
    }
}
