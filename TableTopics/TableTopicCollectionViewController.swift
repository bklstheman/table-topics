//
//  TableTopicCollectionViewController.swift
//  TableTopics
//
//  Created by William Kluss on 6/4/16.
//
//

import UIKit
import Foundation

class TableTopicCollectionViewController: UICollectionViewController, TableTopicCollectionViewCellDelegate, AddTableTopicViewControllerDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate {

    var category: Category! //Forcing unwrap not the greatest but you know...yolo
    fileprivate var tableTopics: [TableTopic]!
    fileprivate let tableTopicService = TableTopicService()
    private let notificationCenter = NotificationCenter.default
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        title = category.title
        reloadTopicData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let count = category.tabletopics?.count else {
            return 0
        }
        
        return count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopicCell", for: indexPath) as? TableTopicCollectionViewCell else {
            return UICollectionViewCell() //NOTE: This should never happen but I guess its safer then force unwrapping.
        }
        let tableTopic = tableTopics[indexPath.row]
        
        cell.topicDescription.text = tableTopic.topicDescription
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout

        let numberofItem: CGFloat = UIDevice.current.userInterfaceIdiom == .phone ? 2 : 3

        let collectionViewWidth = self.collectionView.bounds.width

        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing

        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left

        let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)
        
        let topic = tableTopics[indexPath.row]
        
            
        guard let topicDesc = topic.topicDescription else {
            return CGSize(width: 240, height: 180)
        }

        let topicHeight = sizeForTopicHeight(for: topicDesc)

        return CGSize(width: width, height: Int(topicHeight))
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addTopicSegue" {
            let addViewController = segue.destination as! AddTableTopicViewController
            addViewController.delegate = self
            addViewController.category = category
        }
    }
    
    func deleteTableTopicWasPressed(_ tableTopicCell: TableTopicCollectionViewCell) {
        if let indexPath = collectionView?.indexPath(for: tableTopicCell) {
            let topicToDelete = tableTopics[indexPath.row]
            
            do {
                try tableTopicService.deleteTableTopic(topicToDelete)
                reloadTopicData()
                self.notificationCenter.post(name: Notification.Name.topicStatusUpdated, object: nil)
                collectionView?.reloadData()
            } catch {
                NSLog("Unable to delete topic")
            }
        }
    }
    
    func tableTopicWasSaved(_ category: Category) {
        collectionView?.reloadData()
    }
    
    fileprivate func reloadTopicData() -> Void {
        guard let topics = category.tabletopics?.allObjects as? [TableTopic] else {
            tableTopics = []
            return
        }
        
        tableTopics = topics
        collectionView.reloadData()
    }
    
    fileprivate func sizeForTopicHeight(for text: String) -> CGFloat {
        let font = UIFont.systemFont(ofSize: 17.0)
        let constraintRect = CGSize(width: 240, height: CGFloat.greatestFiniteMagnitude)
        let rect = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        let topicHeight = rect.size.height < 180 ? 180 : (rect.size.height + 60.0)
        
        return topicHeight
    }
    
    @IBAction func addTopicButtonPressed(_ sender: Any) {
        let topicAlertBox = UIAlertController(title: "Enter Topic", message: nil, preferredStyle: UIAlertController.Style.alert)
        
        topicAlertBox.addTextField { (textField: UITextField) in
            textField.delegate = self
            textField.autocapitalizationType = .sentences
            textField.addTarget(self, action: #selector(TableTopicCollectionViewController.topicTextFieldDidChange), for: UIControl.Event.editingChanged)
        }
        
        let addCategoryAction = UIAlertAction(title: "Save", style: UIAlertAction.Style.default) { (action: UIAlertAction) in
            if let textField = topicAlertBox.textFields?.first {
                if let title = textField.text {
                    do {
                        AnalyticManager.sharedInstance.trackEvent(AnalyticKeys.Category.topicAction.rawValue, parameters: [AnalyticKeys.EventAction.saveTopic.rawValue: title])
                        try self.tableTopicService.createTableTopic(title, category: self.category)
                        self.notificationCenter.post(name: Notification.Name.topicStatusUpdated, object: nil)
                        self.reloadTopicData()
                    } catch {
                        NSLog("Unable to create Category")
                    }
                }
            }
        }
        
        addCategoryAction.isEnabled = false
        
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        topicAlertBox.addAction(addCategoryAction)
        topicAlertBox.addAction(cancelAction)
        
        present(topicAlertBox, animated: true, completion: nil)
    }
    
    @objc fileprivate func topicTextFieldDidChange() -> Void {
        if let topicAlertBox = presentedViewController as? UIAlertController {
            if let textField = topicAlertBox.textFields?.first {
                if let action = topicAlertBox.actions.first {
                    if textField.text?.isEmpty == false {
                        action.isEnabled = true
                    } else {
                        action.isEnabled = false
                    }
                }
            }
        }
    }
}
