//
//  AnalyticManager.swift
//  TableTopics
//
//  Created by William Kluss on 5/7/17.
//
//

import Foundation
import Flurry_iOS_SDK

struct AnalyticManager {
    static let sharedInstance = AnalyticManager()
    
    private init() {
        Flurry.startSession("9WZM222BCSR95CWYMCWB", with: FlurrySessionBuilder
        .init()
        .withCrashReporting(true)
        .withLogLevel(FlurryLogLevelAll))
    }
    
    func trackEvent(_ event: String, parameters: [String: Any]?) {
        Flurry.logEvent(event, withParameters: parameters)
    }
}
