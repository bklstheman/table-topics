//
//  StoreReview.swift
//  Topic Master
//
//  Created by William Kluss on 5/9/20.
//

import Foundation

import Foundation
import StoreKit

struct StoreReview {
    private let info = UserInfo()

    func displayReviewPromptIfNeeded() {
        //If we have a time we reviewed already
        //And its a different version then last time they reviewed
        if let dateChecked = info.lastTimeRequestedReview() {
            //we have a date see if toady is 4 months has passed.
            let cal = Calendar(identifier: .gregorian)
            let futureDate = cal.date(byAdding: .month, value: 4, to: dateChecked)!

            //TODO: Check app version is different then last time.
            if (Date() > futureDate && checkIfNumberOfTimesHasMatched()) {
                requestReview()
            }
        } else {
            //First time check, see if the app was open three times.
            //check if its been 3 times.
            if checkIfNumberOfTimesHasMatched() {
                requestReview()
            }
        }
    }
    
    private func requestReview() {
        if let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            info.saveLastReviewedAppVersion(version)
        }
        info.saveNumberOfTimesOpened(0)
        info.saveLastTimeRequestedReview(Date())
        SKStoreReviewController.requestReview()
    }
    
    private func checkIfNumberOfTimesHasMatched() -> Bool {
        var numberOfTimesViewed = info.numberOfTimesOpened()
        numberOfTimesViewed += 1
        info.saveNumberOfTimesOpened(numberOfTimesViewed)
        return numberOfTimesViewed >= 3
    }
}

private enum UserInfoKeys: String {
    case timesOpen = "timesOpen"
    case lastTimeOpened = "lastTimeOpened"
    case lastReviewedAppVersion = "lastReviewedAppVersion"
    case lastTimeSubscriptionChecked = "lastTimeSubscriptionChecked"
}

struct UserInfo {
    private let storage = UserDefaults.standard
    public let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }()
    
    func numberOfTimesOpened() -> Int {
        let timesOpen = storage.integer(forKey: UserInfoKeys.timesOpen.rawValue)
        return timesOpen
    }
    
    func saveNumberOfTimesOpened(_ number: Int) {
        storage.set(number, forKey: UserInfoKeys.timesOpen.rawValue)
    }
    
    func resetNumberOfTimesOpened() {
        storage.removeObject(forKey: UserInfoKeys.timesOpen.rawValue)
    }
    
    func lastTimeRequestedReview() -> Date? {
        guard let lastTimeOpened = storage.string(forKey: UserInfoKeys.lastTimeOpened.rawValue), let dateLastRequested = dateFormatter.date(from: lastTimeOpened) else {
            return nil
        }
        return dateLastRequested
    }
    
    func saveLastTimeRequestedReview(_ date: Date) {
        let dateString = dateFormatter.string(from: date)
        storage.set(dateString, forKey: UserInfoKeys.lastTimeOpened.rawValue)
    }
    
    func lastReviewedAppVersion() -> String? {
        let appVersion = storage.string(forKey: UserInfoKeys.lastReviewedAppVersion.rawValue)
        return appVersion
    }
    
    func saveLastReviewedAppVersion(_ version: String) {
        storage.setValue(version, forKey: UserInfoKeys.lastReviewedAppVersion.rawValue)
    }
}
