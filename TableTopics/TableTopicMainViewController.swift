//
//  TableTopicMainViewController.swift
//  TableTopics
//
//  Created by William Kluss on 6/5/16.
//
//

import UIKit
import Foundation

enum TimerState {
    case running
    case notRunning
}

class TableTopicMainViewController: UIViewController {
    
    @IBOutlet private weak var topicContainer: TopicContainer!
    @IBOutlet private weak var timerButton: UIButton!
    @IBOutlet private weak var timerLabel: UILabel!
    @IBOutlet private weak var resetButton: UIBarButtonItem!
    @IBOutlet private weak var menuButton: UIBarButtonItem!
    @IBOutlet private var cardGesture: UITapGestureRecognizer!
    @IBOutlet private weak var topicDescriptionLabel: UILabel!
    
    private let analyticManager = AnalyticManager.sharedInstance
    private let topicService = TableTopicService()
    private let settingManger = SettingsManager()
    private var timerState: TimerState = .notRunning {
        didSet {
            let enable = timerState == .notRunning
            resetButton.isEnabled = enable
            menuButton.isEnabled = enable
            cardGesture.isEnabled = enable
        }
    }
    
    private var topics: [TableTopic]!
    private var topicTime: Int {
        get {
            return self.settingManger.currentTimerSetting().rawValue
        }
    }
    
    private lazy var stopwatch = Stopwatch(timeUpdated: { [weak self] timeInterval in
        guard let strongSelf = self else { return }
        let newValue = Double(strongSelf.topicTime) - timeInterval
        strongSelf.updateTimerLabel(newTime: newValue)
    })
    
    private let notificationCenter: NotificationCenter = NotificationCenter.default
    
    private func updateTimerLabel(newTime: TimeInterval) {
        timerLabel.text = timeString(from: newTime)
        if newTime == 0 {
            stopwatch.stop()
            blinkTimerLabel {
                self.stopTimer() //weak self dance
                self.timerState = .notRunning
            }
        }
    }
    
    private let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "m:ss"
        return formatter
    }()
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTopicView()
        setupButton()
        topicDescriptionLabel.text = "Tap to Display Topic"
        timerLabel.text = timeString(from: Double(settingManger.currentTimerSetting().rawValue))
        notificationCenter.addObserver(self, selector: #selector(timerSettingUpdated(notification:)), name: .timerSettingUpdate, object: nil)
        notificationCenter.addObserver(self, selector: #selector(topicsUpdated(notification:)), name: .topicStatusUpdated, object: nil)
    }
    
    @objc private func timerSettingUpdated(notification: Notification) {
        timerLabel.text = timeString(from: Double(settingManger.currentTimerSetting().rawValue))
    }
    
    @objc private func topicsUpdated(notification: Notification) {
        do {
            topics = try topicService.retrieveAllTopics()
        } catch {
            NSLog("Unable to load data")
        }    }
    
    private func setupButton() {
        timerButton.tintColor = UIColor.white
        timerButton.backgroundColor = UIColor.systemGreen
        timerButton.layer.cornerRadius = 0.5 * timerButton.bounds.size.width
        timerButton.clipsToBounds = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        do {
            topics = try topicService.retrieveAllTopics()
        } catch {
            NSLog("Unable to load data")
        }
        StoreReview().displayReviewPromptIfNeeded()
    }
    
    @IBAction private func resetButtonPressed(_ sender: UIBarButtonItem) {
        self.analyticManager.trackEvent(AnalyticKeys.EventAction.resetTapped.rawValue, parameters: nil)
        topicDescriptionLabel.text = "Tap to Display Topic"
        
        reloadTopicList()
    }
    
    @IBAction func topicContainerTapped(_ sender: UITapGestureRecognizer) {
        self.analyticManager.trackEvent(AnalyticKeys.EventAction.topicTapped.rawValue, parameters: nil)

        guard topics.count > 0 else {
            let alertController = UIAlertController(title: "Empty Topics", message: "You do not have any topics to display. Tap on Menu to add Topics.", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: nil)
            
            alertController.addAction(cancelAction)
            present(alertController, animated: true, completion: nil)
            return
        }
        selectTopic()
    }
    
    @IBAction func timerButtonTapped(_ sender: AnyObject) {
        if timerButton.titleLabel?.text == "Start" {
            stopwatch.toggle()
            timerButton.backgroundColor = UIColor.red
            timerButton.setTitle("Stop", for: UIControl.State())
            timerState = .running
        } else if timerButton.titleLabel?.text == "Stop" {
            stopTimer()
            stopwatch.stop()
            timerState = .notRunning
        }
    }
    
    private func stopTimer() {
        timerButton.setTitle("Start", for: UIControl.State())
        timerButton.backgroundColor = UIColor.systemGreen
        timerLabel.text = timeString(from: Double(settingManger.currentTimerSetting().rawValue))
    }
    
    func blinkTimerLabel(completion: (() -> Void)?) {
        timerLabel.alpha = 0.0
        UIView.animate(withDuration: 1, delay: 0.0, options: UIView.AnimationOptions(), animations: {
            self.timerLabel.alpha = 1.0
        }, completion: { (Bool) in
            self.timerLabel.alpha = 0.0
            UIView.animate(withDuration: 1, delay: 0, options: UIView.AnimationOptions(), animations: {
                self.timerLabel.alpha = 1.0
            }, completion: { (Bool) in
                self.timerLabel.alpha = 0.0
                UIView.animate(withDuration: 1, delay: 0, options: UIView.AnimationOptions(), animations: {
                    self.timerLabel.alpha = 1.0
                }, completion: { (Bool) in
                    self.timerLabel.alpha = 1.0
                    completion?()
                })
            })
        })
    }
    
    private func setupTopicView()  {
        topicContainer.layer.cornerRadius = 8.0
        topicContainer.layer.masksToBounds = true
    }
    
    override var canBecomeFirstResponder : Bool {
        return true
    }
    
    private func selectTopic() {
        if topics.count == 0 {
            return
        }
        
        let topicIndex = Int(arc4random()) % topics.count
        let topic = topics[topicIndex]
        
        topicDescriptionLabel.text = topic.topicDescription
        topics.remove(at: topicIndex)
        
        if topics.count == 0 {
            reloadTopicList()
        }
    }
    
    private func reloadTopicList() {
        do {
            topics = try topicService.retrieveAllTopics()
        } catch {
            NSLog("Unable to reload data")
            topics = []
        }
    }
    
    private func timeString(from timeInterval: TimeInterval) -> String {
        let seconds = Int(timeInterval.truncatingRemainder(dividingBy: 60))
        let minutes = Int(timeInterval.truncatingRemainder(dividingBy: 60 * 60) / 60)
        return String(format: "%.2d:%.2d", minutes, seconds)
    }
}

class TopicContainer: CardView {
}
