//
//  TopicPackViewContainer.swift
//  Topic Master
//
//  Created by William Kluss on 5/7/20.
//

import UIKit

class TopicPackViewContainer: UIView {
    struct ViewModel {
        let productIds: [TopicPackProductId]
    }

    private let stackView = UIStackView()
    private let viewModel: TopicPackViewContainer.ViewModel
    private weak var delegate: TopicPackViewDelegate?
    
    init(viewModel: TopicPackViewContainer.ViewModel, delegate: TopicPackViewDelegate?) {
        self.viewModel = viewModel
        self.delegate = delegate
        super.init(frame: .zero)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setup() {
        setupStackView()
        setupTopicViews()
    }
    
    private func setupStackView() {
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 5.0
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(stackView)
        stackView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    private func setupTopicViews() {
        for productId in viewModel.productIds {
            let view = TopicPackView(productId: productId)
            view.delegate = delegate
            stackView.addArrangedSubview(view)
        }
    }
}

extension TopicPackProductId {
    func title() -> String {
        switch self {
        case .health:
            return "Health Pack"
        case .personal1:
            return "Personal Pack 1"
        case .personal2:
            return "Personal Pack 2"
        case .deepQuestion1:
            return "Deep Question Pack 1"
        case .deepQuestion2:
            return "Deep Question Pack 2"
        case .education:
            return "Education Pack"
        case .controversial:
            return "Controversial Pack"
        case .personalAll:
            return "Personal Pack 1 & 2"
        case .deepQuestionAll:
            return "Deep Question Pack 1 & 2"
        case .allProducts:
            return "All Pack"
        case .iceBreakerAll:
            return "Ice Breaker Pack 1 & 2"
        case .iceBreaker1:
            return "Ice Breaker Pack 1"
        case .iceBreaker2:
            return "Ice Breaker Pack 2"
        }
    }
}
