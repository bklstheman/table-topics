//
//  SettingsManager.swift
//  TableTopics
//
//  Created by William Kluss on 7/4/16.
//
//

import Foundation


enum TimerSetting: Int {
    case thirty = 30
    case sixty = 60
    case ninety = 90
}

///Controls User settings. Currently used to set what the timer is for.
struct SettingsManager {
    
    fileprivate let safe = UserDefaults.standard
    fileprivate let TimerKey = "timer"
    
    func saveTimerSettings(_ setting: TimerSetting) {
        safe.set(setting.rawValue, forKey: TimerKey)
        safe.synchronize()
    }
    
    func currentTimerSetting() -> TimerSetting {
        if let timerValue = TimerSetting(rawValue: safe.integer(forKey: TimerKey)) {
            return timerValue
        } else {
            return TimerSetting.thirty
        }
    }
}
