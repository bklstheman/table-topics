//
//  IAPHelper.swift
//  Topic Master
//
//  Created by William Kluss on 5/6/20.
//

import Foundation
import StoreKit

protocol PaymentDelegate: class {
    func paymentProcessFinished(transaction: SKPaymentTransaction)
    func restoreProcessFinished(transactions: [SKPaymentTransaction])
}

class IAPHelper: NSObject {
    public typealias ProductsRequestCompletionHandler = (Result<[SKProduct], Error>) -> Void
    private let paymentQueue: SKPaymentQueue
    weak var delegate: PaymentDelegate?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?

    
    init(paymentQueue: SKPaymentQueue = SKPaymentQueue.default()) {
        self.paymentQueue = paymentQueue
        super.init()
        self.paymentQueue.add(self)
    }
    
    deinit {
        self.paymentQueue.remove(self)
    }
    
    func canMakePayments() -> Bool {
        return SKPaymentQueue.canMakePayments()
    }
    
    func buy(_ payment: SKPayment) {
        paymentQueue.add(payment)
    }
    
    func restorePurchases() {
        paymentQueue.restoreCompletedTransactions()
    }
    
    func requestProduct(productId: String, completionHandler: @escaping (Result<[SKProduct], Error>) -> ()) {
        productsRequestCompletionHandler = completionHandler
        let productRequest = SKProductsRequest(productIdentifiers: Set([productId]))
        productRequest.delegate = self
        productRequest.start()
    }
}

extension IAPHelper: SKPaymentTransactionObserver {
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                complete(transaction: transaction)
            case .failed:
                fail(transaction: transaction)
            case .restored:
                restore(transaction: transaction)
            case .deferred: //TODO:
                continue
            case .purchasing: //TODO:
                continue
            @unknown default:
               continue
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        delegate?.restoreProcessFinished(transactions: queue.transactions)
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        delegate?.paymentProcessFinished(transaction: transaction)
        paymentQueue.finishTransaction(transaction)
    }

    private func restore(transaction: SKPaymentTransaction) {
        delegate?.paymentProcessFinished(transaction: transaction)
        paymentQueue.finishTransaction(transaction)
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        if let transactionError = transaction.error as NSError?,
            let localizedDescription = transaction.error?.localizedDescription,
            transactionError.code != SKError.paymentCancelled.rawValue {
            print("Transaction Error: \(localizedDescription)")
        }
        
        delegate?.paymentProcessFinished(transaction: transaction)
        paymentQueue.finishTransaction(transaction)
    }
}

extension IAPHelper: SKProductsRequestDelegate {
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let products = response.products
        productsRequestCompletionHandler?(.success(products))//This doesnt seem to ever really fail? 
        productsRequestCompletionHandler = nil
    }
}

enum TopicPackProductId: String, CaseIterable {
    case health = "com.wek.TableTopicMaster.HealthFinal"
    case personal1 = "com.wek.TableTopicMaster.Personal1Final"
    case personal2 = "com.wek.TableTopicMaster.Personal2Final"
    case deepQuestion1 = "com.wek.TableTopicMaster.DeepQuestions1Final"
    case deepQuestion2 = "com.wek.TableTopicMaster.DeepQuestions2Final"
    case education = "com.wek.TableTopicMaster.EducationFinal"
    case iceBreakerAll = "com.wek.TableTopicMaster.IceBreakersAll"
    case iceBreaker1 = "com.wek.TableTopicMaster.IceBreakers1Final"
    case iceBreaker2 = "com.wek.TableTopicMaster.IceBreakers2Final"
    case controversial = "com.wek.TableTopicMaster.ControversialFinal"
    case personalAll = "com.wek.TableTopicMaster.PersonalAll"
    case deepQuestionAll = "com.wek.TableTopicMaster.DeepQuestion"
    case allProducts = "com.wek.TableTopicMaster.All"
    
    func tableMasterOption() -> [TableMasterOption] {
        switch self {
        case .health:
            return [.health]
        case .personal1:
            return [.personal1]
        case .personal2:
            return [.personal2]
        case .deepQuestion1:
            return [.deepQuestion1]
        case .deepQuestion2:
            return [.deepQuestion2]
        case .education:
            return [.education]
        case .controversial:
            return [.controversial]
        case .personalAll:
            return [.personal1, .personal2]
        case .deepQuestionAll:
            return [.deepQuestion1, .deepQuestion2]
        case .allProducts:
            return TableMasterOption.allCases
        case .iceBreakerAll:
            return [.iceBreaker1, .iceBreaker2]
        case .iceBreaker1:
            return [.iceBreaker1]
        case .iceBreaker2:
            return [.iceBreaker2]
        }
    }
}
