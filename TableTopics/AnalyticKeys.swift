//
//  AnalyticKeys.swift
//  TableTopics
//
//  Created by William Kluss on 5/7/17.
//
//

import Foundation

struct AnalyticKeys {
    enum ScreenName: String {
        case home = "Home Screen"
        case howTo = "How To Screen"
        case menu = "Menu Screen"
        case nameList = "Name List Screen"
        case categoryList = "Category List Screen"
        case topicList = "Topic List Screen"
        case createTopic = "Create Topic Screen"
    }
    
    enum EventAction: String {
        case shake = "Shake"
        case nameTapped = "Name Tapped"
        case topicTapped = "Topic Tapped"
        case resetTapped = "Reset Tapped"
        case howToTapped = "How To Tapped"
        case optionTapped = "Option Tapped"
        case startTapped = "Start Tapped"
        case stopTapped = "Stop Tapped"
        case menuTapped = "Menu Tapped"
        case namesTapped = "Names Tapped"
        case topicsTapped = "Topics Tapped"
        case contactTapped = "Contact Tapped"
        case shareTapped = "Share Tapped"
        case selectName = "Select Name"
        case addName = "Add Name"
        case saveName = "Save Name"
        case deleteName = "Delete name"
        case addCategory = "Add Category"
        case deleteCategory = "Delete Category"
        case saveCategory = "Save Category"
        case deleteTopic = "Delete Topic"
        case saveTopic = "Save Topic"
        case settingsTapped = "Settings Tapped"
        case thirtySecondsTapped = "Thirty Seconds Tapped"
        case sixtySecondsTapped = "Sixty Seconds Tapped"
        case ninetySecondsTapped = "Ninety Seconds Tapped"
        case cancelTapped = "Cancel Tapped"

    }
    
    enum Category: String {
        case topicSelector = "Topic Selector" //used to know how user selects topics, like tapping name, topic or shake, or reset
        case timeOption = "Time Option" //Used to know which option is picked
        case homeAction = "Home Action" //events that are triggered on screen
        case menuAction = "Menu Action" //events done on menu screen
        case nameAction = "Name Action" //Events done on event scree
        case topicAction = "Topic Action" //Actions done when on the topic screen
        case categoryAction = "Category Action"
    }
}
