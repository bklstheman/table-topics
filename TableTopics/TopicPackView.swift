//
//  TopicPackView.swift
//  Topic Master
//
//  Created by William Kluss on 5/7/20.
//

import UIKit

protocol TopicPackViewDelegate: class {
    func didPressTopic(_ value: TopicPackProductId)
}

class TopicPackView: CardView {
    private let button: UIButton = UIButton()
    weak var delegate: TopicPackViewDelegate?
    private let productId: TopicPackProductId
    

    init(productId: TopicPackProductId) {
        self.productId = productId
        super.init(frame: CGRect.zero)
        setupButton()
        backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupButton() {
        button.setTitle(productId.title(), for: .normal)
        button.setTitleColor(UIColor.tableTopicGreen(), for: .normal)
        button.titleLabel?.numberOfLines = 0
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.translatesAutoresizingMaskIntoConstraints = false
        addSubview(button)
        
        button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        let constraints = [button.leadingAnchor.constraint(equalTo: leadingAnchor),
                           button.trailingAnchor.constraint(equalTo: trailingAnchor),
                           button.topAnchor.constraint(equalTo: topAnchor),
                           button.bottomAnchor.constraint(equalTo: bottomAnchor)]
        
        NSLayoutConstraint.activate(constraints)
    }
    
    @objc private func buttonTapped() {
        delegate?.didPressTopic(productId)
    }
}
