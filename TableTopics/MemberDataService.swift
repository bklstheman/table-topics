//
//  MemberDataService.swift
//  TableTopics
//
//  Created by William Kluss on 5/30/16.
//
//

import Foundation
import CoreData


enum ResultsError: Error {
    case noResults
    case tooManyResults
}

class MemberDataService {

    fileprivate let managedContext = CoreDataManager.sharedInstance.managedObjectContext
    fileprivate let memberFetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Member")
    fileprivate let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
    
    func retrieveAllMembers() throws -> [Member] {
        let fetchRequest = memberFetchRequest
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        
        let results = try managedContext.fetch(fetchRequest)
        
        if let members = results as? [Member] {
            return members
        } else {
            NSLog("Retrieved wrong type of members. Should not have happen")
            return []
        }
    }
    
    func retrieveSelectedMembers() throws -> [Member] {
        let fetchRequest = memberFetchRequest
        fetchRequest.sortDescriptors = [nameSortDescriptor]
        
        let selectedPredicate = NSPredicate(format: "isMemberSelected = %@", NSNumber(value: true as Bool))
        fetchRequest.predicate = selectedPredicate
        
        let results = try managedContext.fetch(fetchRequest)
        
        if let members = results as? [Member] {
            return members
        } else {
            NSLog("Retrieved wrong type of members. Should not have happen")
            return []
        }
    }
    
    func retrieveMember(_ name: String) throws -> Member {
        let fetchRequest = memberFetchRequest
        let namePredicate = NSPredicate(format: "name == %@", name)
        
        fetchRequest.predicate = namePredicate
        
        let results = try managedContext.fetch(fetchRequest)
    
        if let member = results.first as? Member, results.count == 1 {
            return member
        } else {
            if results.count == 0 {
                throw ResultsError.noResults
            } else {
                throw ResultsError.tooManyResults
            }
        }
    }
    
    func createMember(_ name: String) throws {
        let newMember = NSEntityDescription.insertNewObject(forEntityName: "Member", into: managedContext) as? Member
        
        newMember?.name = name
        newMember?.isMemberSelected = true
        
        try managedContext.save();
    }
    
    func deleteMember(_ member: Member) throws {
        managedContext.delete(member)
        
        try managedContext.save()
    }
    
    func updateMember(_ member: Member) throws {
        try managedContext.save()
    }
    
    
}
