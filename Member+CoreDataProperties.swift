//
//  Member+CoreDataProperties.swift
//  Topic Master
//
//  Created by William Kluss on 5/8/20.
//
//

import Foundation
import CoreData


extension Member {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Member> {
        return NSFetchRequest<Member>(entityName: "Member")
    }

    @NSManaged public var firstName: String?
    @NSManaged public var isMemberSelected: NSNumber?
    @NSManaged public var lastName: String?
    @NSManaged public var name: String?
    @NSManaged public var timesCalledOn: NSNumber?

}
