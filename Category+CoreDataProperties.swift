//
//  Category+CoreDataProperties.swift
//  Topic Master
//
//  Created by William Kluss on 5/8/20.
//
//

import Foundation
import CoreData


extension Category {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Category> {
        return NSFetchRequest<Category>(entityName: "Category")
    }

    @NSManaged public var title: String?
    @NSManaged public var productId: String?
    @NSManaged public var tabletopics: NSSet?

}

// MARK: Generated accessors for tabletopics
extension Category {

    @objc(addTabletopicsObject:)
    @NSManaged public func addToTabletopics(_ value: TableTopic)

    @objc(removeTabletopicsObject:)
    @NSManaged public func removeFromTabletopics(_ value: TableTopic)

    @objc(addTabletopics:)
    @NSManaged public func addToTabletopics(_ values: NSSet)

    @objc(removeTabletopics:)
    @NSManaged public func removeFromTabletopics(_ values: NSSet)

}
